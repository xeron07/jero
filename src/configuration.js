/**
 * this class used for structring conferecne variables
 * @importans or config variables are created here
 * @variables can be set and found by using this class
 * This the main root class
 */

export default class Configuration {
  /**
   * @constructor
   * @param none
   */
  constructor() {
    //start of conference method

    //use to config about browser & screen share
    this.initOptions = {
      // 1. JitsiMeetJS.init(initOptions);

      disableAudioLevels: false,

      // The ID of the jidesha extension for Chrome.
      desktopSharingChromeExtId: "mbocklcggfhnbahlnepmldehdhpjfcjp",

      // Whether desktop sharing should be disabled on Chrome.
      desktopSharingChromeDisabled: false,

      // The media sources to use when using screen sharing with the Chrome
      // extension.
      desktopSharingChromeSources: ["screen", "window"],

      // Required version of Chrome extension
      // this.size = { height: "700", width: "700" };
      // this.node = "div[0]";
      desktopSharingChromeMinExtVersion: "0.1",

      // Whether desktop sharing should be disabled on Firefox.
      desktopSharingFirefoxDisabled: true
    };

    /**
     * @most @important object for server site connection
     * by this object we can decide were to create and  config our conferce
     * meet.jit.si is the default server provide jitsi group
     */

    this.option = {
      /** ===================@option object started here================== */

      // 2. connection=new JitsiMeetJS.JitsiConnection(null,null,option);

      /**
       * @jitsi beta server
       * to test in globalk beta server
       */

      // hosts: {
      //   domain: "fuji",
      //   muc: "conference.fuji",
      //   // anonymousdomain: "fuji"
      // },
      // bosh: "//fuji/http-bind"

      // hosts: {
      //   domain: "meet.jit.si",
      //   muc: "conference.meet.jit.si",
      //   anonymousdomain: "meet.jit.si"
      // },
      // bosh: "//meet.jit.si/http-bind"

      hosts: {
        domain: "meet.jitsi",
        muc: "muc.meet.jitsi"
      },
      bosh: "https://fuji/http-bind"

      // hosts: {
      //   domain: "meet.jit.si",
      //   muc: "conference.meet.jit.si",
      //   anonymousdomain: "meet.jit.si"
      // },
      // bosh: "https://meet.jit.si/http-bind"

      /**
       * @development server
       */

      // hosts: {
      //   domain: "192.168.0.147",
      //   muc: "conference.192.168.0.147",
      //   anonymousdomain: "192.168.0.147"
      // },
      // bosh: "https://192.168.0.147:8443/http-bind"

      /** ============@option object end here============= */
    };

    this.confOption = {
      // 3. room=connection.initJitsiConference("new13",confOption);
      openbridgeChannel: true
    };

    /**
     * this object is to config
     * @user role
     * @ui node
     * @UI node/div size (height ,width)
     * @room name (confercne room name)
     *  dont confuse with option object
     * where @option object used for configuring server
     *  but here @optionObj used for @UI and other internal implementation
     */
    this.optionObj = {
      size: { height: "700", width: "700" },
      roomName: "newroom",
      node: "div[0]",
      role: "student"
    };

    // this.size = { height: "700", width: "700" };
    // this.node = "div[0]";
    this.browserObj = "chrome";

    /** @default no moderator so moderator variable is null */
    this.moderator = null;

    /** @default local user id is null */
    this.userId = null;
    // this.roomName = "newroom";
  } //end of conference

  /**
   * Setter method for all the variable for the conference
   * all @variables have there default value
   * some of them are part of a object variable
   */

  /**
   *User id setter method
   *@param a single string variable
   */
  setUserId = i => {
    this.userId = i;
  };

  /**
   * Initial object variable setter
   * this valriable used during connection
   * @param a single object variable
   */
  setInitOptions = i => {
    this.initOptions = i;
  };

  /**
   * Setting conferece moderator
   * @param a single jid(jitsi id) or string value
   */
  setModerator = m => {
    this.moderator = m;
  };

  /**
   * server information setter method
   * @param a single  object
   */
  setOption = o => {
    this.option = o;
  };

  /**
   * conference option setter
   * @param a single object value
   */
  setConfOption = c => {
    this.confOption = c;
  };

  /**
   * conference div size setter (curently not used )
   * @param a single object
   */
  setSize = s => {
    this.optionObj.size.height = s.height;
    this.optionObj.size.width = s.width;
  };

  /**
   * conference node / div setter for user interface
   * @(currently not used)
   * @param a sigle string
   */
  setNode = n => {
    this.optionObj.node = n;
  };

  /**
   * conference room name setter
   * @param a single string
   *
   */
  setRoomName = r => {
    this.optionObj.roomName = r;
  };

  /**
   * user role for the joined conference
   * @param a single string value
   */
  setRole = r => {
    this.optionObj.role = r;
  };

  /**
   * getter methods for all varibles for this conference
   * all the variables here has a default value / null value
   * @after the joining the conference all the variable updated (if needed)
   * conference information or used variable values can be found from this methods
   */

  /**
   * returns  current user id
   * @returend value is a jid(jitsi id)
   * @type string
   */
  getUserId = () => {
    return this.userId;
  };

  /**
   * return initial option object
   * @returns initalOptions which will  be used during connection
   * @type object
   */
  getInitOptions = () => {
    return this.initOptions;
  };

  /**
   * moderator id canbe found using this method
   * @returns moderator jid(jitsu id)
   * @type string
   */
  getModerator = m => {
    return this.moderator;
  };

  /**
   * Option  object can be found using this method
   * @returns a oprion object which will be used during creating conference
   * @type object
   */
  getOption = () => {
    return this.option;
  };

  /**
   * conferecne object used for creating jitsi conferecne object
   * canbe found using this method
   * @returns conferece object
   * @type a single object
   */
  getConfOption = () => {
    return this.confOption;
  };

  /**
   * node/div size canbe found using this method
   * @warning (currently not used)
   * @returns node / div size(height width)
   * @type a single object
   */
  getSize = () => {
    return this.optionObj.size;
  };

  /**
   * node/div name canbe found  using this method
   * @waring (currently not used)
   * @returns the name of the node / div
   * @type a single string
   */
  getNode = () => {
    return this.optionObj.node;
  };

  /**
   * conference room name canbe found using this method
   * @returns conference room name
   * @type a single string
   */
  getRoomName = () => {
    return this.optionObj.roomName;
  };

  /**
   * currenty user (local user ) role of this conference canbe found using this method
   * @returns currenct user role
   * @type a single string
   */
  getRole = () => {
    return this.optionObj.role;
  };
}
