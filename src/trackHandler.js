/**
 * importing toaster class & sweetalert for notification purpose
 * importing jitsi library for local track purpose
 */
import Toast from "./toast";
import * as JitsiMeetJS from "../dependency/lib-jitsi-meet.min.js";

import Swal from "sweetalert2";

/**
 * This class handles all the event for track
 * participant track data and option handle from this class
 * local user track create and add in conference from here
 */
export default class TrackHandler {
  /**
   *
   * @param {*} r is a lib class object
   */
  constructor(r) {
    //lib class object
    this.room = r;

    //toast object to show notification
    this.toastr = new Toast();
    this.isSharedScreen = false;
    this.isVideo = true;

    /**
     * this contain  the information of the local user track
     * @type hash map
     * user id is the key for this hash map
     */
    this.remoteTracks = [];

    /**
     * store the local user track data
     * @type array of object
     */
    this.localTracks = [];
  }

  /**
   * on local track start & create this method will
   * store the tracks in class variable
   */
  onLocalTracks = tracks => {
    this.localTracks = tracks;
    // tracks.on(
    //   JitsiMeetJS.events.tracks.LOCAL_TRACK_STOPPED,
    //   this.addLocalTracksToRoom
    // );
  };

  /**
   * Using the RTC module Jitsimeet will start local track
   * by the connected device and create local track to add in conferecne
   */
  createLocalTracks = () => {
    console.warn("ahem");
    JitsiMeetJS.createLocalTracks({ devices: ["audio", "video"] })
      .then(this.onLocalTracks)
      .catch(error => {
        console.error(error);
        throw error;
      });
  };

  /**
   * adding local tracks to room after joining
   * the conferecne room
   */
  addLocalTracksToRoom = () => {
    console.error("i'm here");
    setTimeout(() => {
      if (this.localTracks.length > 0) {
        for (let i = 0; i < this.localTracks.length; i++) {
          this.room.room.addTrack(this.localTracks[i]);
        }
        this.showlocalTrack();
        console.error("local added");
        console.error(this.localTracks);
        this.room.setUserId(this.room.room.myUserId());
      } else {
        this.createLocalTracks();
        this.addLocalTracksToRoom();
      }
    }, 2000);
  };

  /**
   * adding local tracks to DOM div
   * @div id must me "jero-self" or user can't find his local tracks
   */
  showlocalTrack = () => {
    let video = null;

    for (let i = 0; i < this.localTracks.length; i++) {
      if (this.localTracks[i].getType() === "video") {
        this.localTracks[i].attach($("#jero-self")[0]);
      }
    }
    video = document.getElementById("jero-self");

    video.style.transform = "scaleX(-1)";
  };

  /*
   * Track adding event handler
   * @params jitsi track object
   * it only trigger when any kind of track added to conference
   */
  added = track => {
    if (track.isLocal()) {
      return;
    }

    //getting user id from the track object
    let uid = track.getParticipantId();

    if (this.remoteTracks[uid] == null) {
      this.remoteTracks[uid] = [];
    }

    this.remoteTracks[uid].push(track); //participant track is push like a stack
    let i = 1;

    //calling for farther operation after checking
    this.checkDiv(track, uid);

    //selecting the participant to get better video quality
    this.room.room.selectParticipants(this.room.membersKey);
    //this.trackLoop(i);
  };

  //this is only use for developing purpose only
  trackLoop = i => {
    console.error("--------#" + i + "----------");
    console.error(this.remoteTracks);
    i = i + 1;
    setTimeout(() => {
      this.trackLoop(i);
    }, 1000);
  };

  /*
   * checking if video tag is added dynamically in ui or
   * wait for 1 sec
   * if video tag added this function will try to add video and audio to ui
   * @params one is the jitsi track and another is the user/participant id
   */
  checkDiv = (track, uid) => {
    if (track.getType() === "video") {
      setTimeout(() => {
        if (track.getType() === "video") {
          if (typeof document.getElementById("v-" + uid) === "undefined") {
            this.checkDiv(track, uid);
          } else {
            this.addVideo(track);
          }
        }
        // else if (track.getType() === "audio") {
        //   if (typeof document.getElementById("a-" + uid) === "undefined") {
        //     this.checkDiv(track, uid);
        //   } else {
        //     this.addAudio(track);
        //   }
        // }
      }, 1000);
    } else if (track.getType() === "audio") {
      // this.addAudio(track);
    }
  };

  /*
   * adding video to ui
   * @params is a jitsi track (video only) data
   * it's a void function with a single parameter
   * only called when a new video added to conference
   * */
  addVideo = track => {
    let vid = null;
    console.warn(track);
    if (this.room.getRole() === "teacher") {
      track.attach($(`#v-${track.getParticipantId()}`)[0]);
      vid = `v-${track.getParticipantId()}`;
    } else {
      console.warn("in: " + this.room.getModerator());
      if (this.room.getModerator() === track.getParticipantId()) {
        track.attach($(`#v-${track.getParticipantId()}`)[0]);
        vid = `v-${track.getParticipantId()}`;
      }
    }
    //flipping the video for viewer
    if (vid) {
      let video = document.getElementById(vid);

      //filiping vertically only  by one scale
      video.style.transform = "scaleX(-1)";
    }

    this.toastr.success("Your video added to conference");
  };

  /*
   * adding audio to view
   * @params is a jitsi track (audio only) data
   * it's a void function with a parameter
   * only call when new audio added to conference
   * */
  addAudio = track => {
    if (this.room.getRole() === "teacher") {
      track.attach($(`#a-${track.getParticipantId()}`)[0]);
    } else if (this.room.getRole() === "student") {
      if (track.getParticipantId() == this.room.getModerator()) {
        track.attach($(`#a-${track.getParticipantId()}`)[0]);
      }
    }
  };

  /*
   * @Track remove/lost event handler
   * @program will wait seven seconds for the track then it will remove the user
   */
  removed = track => {
    console.warn("track removed: " + track.getParticipantId());
    console.warn(track);
    if (this.isSharedScreen) {
      this.isSharedScreen = false;
      this.addLocalTracksToRoom();
    }

    //  this.room.updateUI();
  };

  /*
   * mute controller for local track
   * @params is a string which is the id of a user
   * this is a void function
   * this function use for muting the user from conference
   * */
  muteToggle = id => {
    let track = null;
    console.error("id from mute :" + id);
    console.error(this.remoteTracks[id]);
    if (this.remoteTracks[id]) {
      track = this.remoteTracks[id];

      for (let i = 0; i < track.length && track[i].getType() == "audio"; i++) {
        if (track[i].isMuted()) {
          track[i].setMute(false);
          this.toastr.success("A user just add audio to conference");
        } else {
          track[i].setMute(true);
          this.toastr.error("A user just muted from conference");
        }
      }
    }
  };

  /* * track mute change event handler
   * @params is a jitsi track
   * it will trigger when a track mute status changed
   *  */
  trackMuteChanged = track => {
    if (track.getParticipantId() === this.room.getUserId()) {
      console.error("You have been muted");
    } else {
      // let element = document.getElementById("m-" + track.getParticipantId());
      // if (track.isMuted()) {
      //   element.className = "fas fa-microphone-alt";
      // } else {
      //   element.className = "fas fa-microphone-alt-slash";
      // }

      Swal.fire("Yahoo", "You muted someone", "success");
    }
  };

  // *** not used for now *** //
  //local track change handler
  // trackPlayToggle = track => {
  //   if (track.paused) {
  //     track.play();
  //   } else {
  //     track.pause();
  //   }
  // };

  /*
   * local track handler
   * @params is a jitsi track (local)
   * it only trigger when any local track stop
   * */
  localTrackStopped = track => {
    console.log(track);
    this.toastr.warning("local track stoped");
  };

  /*
   * local track change handler
   * @params jitsi track (local)
   * it only trigger when any local track changed
   * */
  localTrackChanged = track => {
    console.log(track);

    this.toastr.warning("Local track changed");
  };

  /*
   * reload track if any changes in member number on conference or in ui.
   * this will connect the video to ui after any user left or add
   * */
  reloadTracks = () => {
    if (this.room.membersKey) {
      let data = this.room.membersKey;
      for (let i = 0; i < data.length; i++) {
        if (this.remoteTracks[data[i]]) {
          //Track reloading
          let tracks = this.remoteTracks[data[i]];
          console.error(tracks);
          if (tracks) {
            tracks.forEach(t => {
              this.checkDiv(t, data[i]);
            });

            console.warn(typeof tracks);
          }
        } else {
          let trackData = this.room.members[data[i]]._tracks;
          trackData.forEach(t => {
            if (t) {
              this.checkDiv(t, data[i]);
            }
          });
        }
      }
      // this.checkDiv(
      //   this.remoteTracks[this.room.membersKey[i]],
      //   this.room.membersKey[i]
      // );
    }
  };

  /**
   * Switching media device method
   * switching from default media to screen share
   */
  switchVideo = () => {
    this.isVideo = !this.isVideo;

    //storing the previous local track
    this.rTrack = this.localTracks;
    if (this.localTracks[1]) {
      this.localTracks[1].dispose(); //destroying the previous local track
      this.localTracks.pop();
    }
    JitsiMeetJS.createLocalTracks({
      devices: [this.isVideo ? "video" : "desktop"]
    })
      .then(tracks => {
        this.localTracks.push(tracks[0]);

        //adding event handlers for shared screen track
        this.localTracks[1].addEventListener(
          JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
          () => {
            this.toastr.warning("Screen sharing stopped");
            this.localTracks = [];
            this.addLocalTracksToRoom();
          }
        );

        //adding shared screen track to the local user view
        this.localTracks[1].attach($("#jero-self")[0]);
        let video = document.getElementById("jero-self");
        this.isSharedScreen = true;
        video.style.transform = "scaleX(-1)";
        this.videoSwitched = true;

        //adding track to conference for everyone
        this.room.room.addTrack(this.localTracks[1]);
        console.warn(this.localTracks[this.localTracks.length - 1]);
      })
      .catch(err => {
        console.error(err);
      });
  };
}
