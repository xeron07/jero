/**
 * importing all handlers class and jitsi meet lib
 * JitsiMeetJs object needed to configuring handlers
 *
 */

import TrackHandler from "./trackHandler";
import UserHandler from "./userHandler";
import LibHandler from "./libHandler";
import * as JitsiMeetJS from "../dependency/lib-jitsi-meet.min.js";

/**
 * this class is the middleware for all handlers
 * config the all handlers class methos for each event
 *
 */
export default class Handlers {
  /**
   * @constructor
   * @param {*} lib is a lib class object
   * thius constructor also cnfig the handler class object
   */
  constructor(lib) {
    this.libObj = lib;
    this.trackHandler = new TrackHandler(this.libObj);
    this.userHandler = new UserHandler(this.libObj);
    this.libHandler = new LibHandler(this.libObj);
    //this.errorhandler = new errorHandler();
  }

  /**
   * method for connection handler
   * this method configure the event handler during connection
   * @param Jitsi connection object
   */
  setConnectionHandler = connectionObj => {
    /**
     * @emits when connection success
     */
    connectionObj.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
      this.libHandler.connSuccess
    );

    /**
     * @emits when connection failed
     */
    connectionObj.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_FAILED,
      this.libHandler.connFailed
    );

    /**
     * @emits when connection disconnect with server
     */
    connectionObj.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
      this.libHandler.connLost
    );
  };

  /**
   * this method is to configure the conference event handler
   * @param is a jitsi conferce object
   */
  setConferenceHandler = roomObj => {
    /**
     *  @emits when conference joinning success
     */
    roomObj.on(
      JitsiMeetJS.events.conference.CONFERENCE_JOINED,
      this.libHandler.confJoined
    );

    /**
     *  @emits when user left or forcefully make the user leave the conference
     */
    roomObj.on(
      JitsiMeetJS.events.conference.CONFERENCE_LEFT,
      this.libHandler.confLeft
    );

    /**
     *  @emits when error occures during joinning the conference
     */
    roomObj.on(
      JitsiMeetJS.events.conference.CONNECTION_ERROR,
      this.libHandler.confError
    );

    /**
     *  @emits when conference connection failed
     */
    roomObj.on(
      JitsiMeetJS.events.conference.SETUP_FAILED,
      this.libHandler.confFailed
    );

    /**
     *  @emits whena new user joined the conference successfully
     * @param  user object and id will be return for event handler method
     */
    roomObj.on(
      JitsiMeetJS.events.conference.USER_JOINED,
      this.userHandler.userAdded
    );

    /**
     *  @emits whena a user or participant left the conference successfully
     * @param  user object and id will be return for event handler method
     */
    roomObj.on(
      JitsiMeetJS.events.conference.USER_LEFT,
      this.userHandler.userLeave
    );

    /**
     *  @emits whena a user kicked by the moderator
     */
    roomObj.on(
      JitsiMeetJS.events.conference.KICKED,
      this.userHandler.userKicked
    );

    /**
     *  @emits whena a participant new track added to the conference
     */
    roomObj.on(
      JitsiMeetJS.events.conference.TRACK_ADDED,
      this.trackHandler.added
    );

    /**
     *  @emits whena a participant track removed from the conference
     */
    roomObj.on(
      JitsiMeetJS.events.conference.TRACK_REMOVED,
      this.trackHandler.removed
    );

    /**
     *  @emits whena a participant track mute status changed
     */
    roomObj.on(
      JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED,
      this.trackHandler.trackMuteChanged
    );
  };
}
