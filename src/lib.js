/**
 * We used ECMAScript6
 * some dependency and other module (like class )
 * are imported here
 * Import keyword used for adding dependency
 * @deatis about import can be found here (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
 *
 */

import Configuration from "./configuration.js";
import Handlers from "./handlers";
import * as Swal from "sweetalert2";
import * as JitsiMeetJS from "../dependency/lib-jitsi-meet.min.js";
import UI from "./UI.js";

/**
 * This class is the operation & middleware class
 * The connection and conferece created using this class
 * @class inherits the Configuration class and later inherited by Confernce class
 * @flow of conferece join starts from calling createConnection Method
 */
export default class LibClass extends Configuration {
  /**
   *
   * @constructor
   * @param none
   *
   */
  constructor() {
    super();
    this.connection = null;
    this.room = null;
    this.isLeaved = false;
    this.ui = new UI(this);
    this.isVideo = true;
    this.videoSwitched = false;

    this.handlers = new Handlers(this);

    /**
     * this are the key variables for conferecne
     */

    /**
     * stores the participants information excluding the local user
     * @type array
     * object @typeof data stored by this array
     * @update data during user join and leaved
     */
    this.members = [];

    /**
     * @todo
     * by using this array we can make a map for joined participant
     * this is for future implementation
     */
    this.membersMap = []; // this was used to store the history but currently not used

    /**
     * @most @important variable for ui implementation
     * stores the participants id excluding the local user
     * @type array
     * object @typeof data stored by this array
     * @update data during user join and leaved
     */
    this.membersKey = [];

    this.localTracks = [];

    this.rTrack = [];
  }

  /**
   * creacting connection with jero server
   * we need jitsimeetjs object to initialize and connection
   * initialize option contains conecction information
   */
  createConnection = () => {
    JitsiMeetJS.init(this.initOptions);

    this.connection = new JitsiMeetJS.JitsiConnection(null, null, this.option);
    //connection event handler initialization before connecting
    this.handlers.setConnectionHandler(this.connection);

    //calling local track track function to start local media
    this.handlers.trackHandler.createLocalTracks();
    if (this.getRole() === "teacher") {
      this.addModerator();
    } else {
      Swal.fire({
        input: "text",
        inputPlaceholder: "Enter a room name"
      }).then(re => {
        console.warn(re);
        if (re.value) {
          this.setRoomName(re.value);
          this.connection.connect();
          console.warn(this.connection);
        } else {
          Swal.fire(
            "Oops!",
            "Please enter a room name.\nStart from clicking connect.",
            "error"
          );
        }
      });
    }
  };

  /**
   * If the the user is a teacher he has to
   * give some information before creacting connection and
   * start his class.
   * @username / email
   * @password & @roomname infomation needed to create a room
   */
  addModerator = () => {
    Swal.mixin({
      confirmButtonText: "Next &rarr;",
      showCancelButton: false,
      progressSteps: ["1", "2"]
    })
      .queue([
        {
          title: "Enter your email address",
          input: "email",
          text: "*this will use for you login in conference"
        },
        {
          title: "Enter a password",
          input: "password",
          text: "*this will use for you login in conference"
        }
      ])
      .then(res => {
        Swal.fire({
          input: "text",
          inputPlaceholder: "Enter a room name"
        }).then(re => {
          console.warn(re);
          if (re.value) {
            this.setRoomName(re.value);
            this.connection.connect();
            console.warn(this.connection);
          } else {
            Swal.fire(
              "Oops!",
              "Please enter a room name.\nStart from clicking connect.",
              "error"
            );
          }
        });

        //{ id: res.value[0], password: res.value[1] }
      });
  };

  /**
   * room create function
   * this method use to initialize a room on desire connection
   * and setup the conference events handler and joining the room
   * after room create
   */
  roomCreate = () => {
    this.room = this.connection.initJitsiConference(
      this.getRoomName(),
      this.confOption
    );

    this.handlers.setConferenceHandler(this.room);
    this.roomJoin();
  };

  studentUI = mId => {
    return this.ui.sUI(this.handlers.trackHandler);
  };

  /**
   * Joining the room after creating or finding the conference
   */

  roomJoin = () => {
    console.warn(this.room);
    this.room.join();
    console.warn(this.room);
    console.error(this.room.myUserId());
    this.handlers.trackHandler.addLocalTracksToRoom();
  };

  // setStudentVideos = () => {
  //   let obj = null;
  //   let vid = "";
  //   let v = null;
  //   for (let i = 1; i < this.members.length; i++) {
  //     obj = this.membersMap[this.members[i]];
  //     console.warn(obj._tracks);
  //     if (obj) {
  //       for (let j = 0; j < obj._tracks.length; j++) {
  //         vid = vid = "jeroTeacher-" + this.members[i];
  //         if (obj._tracks[j].getType() === "video") {
  //           v = document.getElementById(vid);
  //           console.warn(obj._tracks[j]);
  //           obj._tracks[j].attach($(`#${vid}`)[0]);
  //           v.play();
  //           console.error(v);
  //         }
  //       }
  //     } else {
  //     }
  //   }
  // };

  // updateUI = () => {
  //   // this.members = mm;

  //   //let mainNode = document.getElementById(this.getNode());
  //   if (this.members.length > 0) {
  //     console.warn("moderator: " + this.getModerator());

  //     if (this.getRole() === "student") {
  //       this.room.selectParticipants([this.getModerator()]);
  //       return;
  //     } else if (this.getRole() === "teacher") {
  //       this.room.selectParticipants(this.members);
  //       let firstBoy = this.membersMap[this.members[0]];
  //       let v = document.getElementById("jeroTeacher-remoteVideoBig");

  //       let parent = document.getElementById("jeroTeacher-leftVideoBox");
  //       let first = parent.querySelectorAll(".jeroTeacher-videoBox");
  //       console.warn(first);
  //       let i = 0;
  //       while (i < first.length) {
  //         parent.removeChild(first[i]);
  //         i++;
  //       }

  //       if (this.members.length > 1) {
  //         parent.innerHTML = this.ui.tUI();
  //         console.warn(this.ui.tUI());

  //         //this.setStudentVideos();
  //       }
  //     }
  //   } else {
  //     console.error("abc empty");
  //   }
  // };
}
