/**
 * importing **strophe(media library) it is one of most important module
 * imported jitsi logger module but not used for now
 */

//const LibClass = require("./lib");
import { getLogger } from "jitsi-meet-logger";
// import * as Strophe from "strophe";
import Toast from "./toast";

import LibClass from "./lib";

/**
 * @extends lib class which also extends configuration class
 * this class used to communicate directly as api class
 * api called handled by this class methods
 */
class Conference extends LibClass {
  /**
   * @constructor
   * @param {*} obj is a object which contain many information(ex. role, roomname etc.)
   * @constructor params used to preset conference for connection with server
   * although many of the data is already has a default value
   * default data will be replaced by given data
   */
  constructor(obj) {
    super(); //calling super class(lib) constructor
    this.optionObj = { ...this.optionObj, ...obj }; //merging default data and given data
    this.addDependency(); //calling to add dependency to DOM
    this.toast = new Toast();
    this.isFullScreen = false; //by default full screen is off
  }

  /**
   * Conference start by calling this method
   * this function create a nd start the conference
   */
  start = () => {
    this.members = [];
    this.isLeaved = false;

    this.createConnection(); //calling to start process of conference

    // this.roomCreate();
  };

  /**
   *  some dependency will be added to Dom through this method
   * this method called at the beginning (in constructor) of class instance creation
   *
   */
  addDependency = () => {
    let link = null;

    let script = document.createElement("script");
    script.src = "https://kit.fontawesome.com/605ed7a303.js";
    document.head.appendChild(script);

    link = document.createElement("link");
    link.setAttribute("rel", "stylesheet");
    link.setAttribute(
      "href",
      // "http://192.168.0.150:4100/jero-v4/toastr.css"
      "https://res.cloudinary.com/emerging-it/raw/upload/v1579954246/cdn/toastr_nwk9lz.css"
    );
    document.getElementsByTagName("head")[0].appendChild(link);
  };

  /**
   * mute the local user audio track
   * this is still in under modification
   */
  muteLocal = id => {
    this.handlers.trackHandler.muteToggle(id);
    this.toast.warning("Your audio track has been muted");
  };

  /**
   * kick the participant by the id
   * @param id is a single jid(jitsi id)
   * @param @type string
   */
  kickParticipant = id => {
    this.room.kickParticipant(id);
    this.toast.success("You kicked someone successfully");
  };

  /**
   * play pause toggler handler
   * @param id is a single jid(jitsi id)
   * @param @type string
   */
  playToggle = id => {
    let v = document.getElementById("v-" + id);
    if (v) {
      if (v.paused) {
        v.play();
      } else {
        v.pause();
      }
    }
  };

  /**
   * use to leave the conference (for local user only)
   */
  leave = () => {
    this.room.leave();
    this.members = [];
    this.membersKey = [];
    this.isLeaved = true;
    this.toast.warning("You left the conference");
  };

  /**
   * disable to send local video track
   * by default at the start of conference local user video will be add to conference
   */
  cancelLocalVideo = () => {
    if (this.localTracks) {
      this.localTracks.forEach(data => {
        if (data.getType() === "video") data.dispose();
      });
    }
    this.toast.warning("You disabled sending video");
  };

  /**
   * disable local audio track to add in conference
   * by default system will get and add local audio track data
   * to the conference
   */
  cancelLocalAudio = () => {
    if (this.localTracks) {
      this.localTracks.forEach(data => {
        if (data.getType() === "audio") data.dispose();
      });
    }
    this.toast.warning(" Your  audio track removed from conferecne");
  };

  /**
   * Showing selected video full screen
   * @param video tag id
   */
  fullScreen = id => {
    if (!document.fullscreenElement) {
      this.createFullScreenView(id);
    } else {
      if (document.exitFullscreen) {
        this.closeFullScreen();
      }
    }
  };

  /**
   * Browser setting for full screen view
   * @param id of video tag
   */

  createFullScreenView = id => {
    let elem = document.getElementById(id);
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      /* Chrome, Safari & Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      /* IE/Edge */
      elem.msRequestFullscreen();
    }
  };

  /**
   * stop the full screen mode
   */
  closeFullScreen = () => {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
  };

  /**
   * swap the membersKey to catch the focus of
   * some user
   */
  swapIndex = val => {
    let i = this.membersKey.indexOf(val);
    console.error(this.membersKey);
    if (i !== -1) {
      console.warn(i);
      [this.membersKey[0], this.membersKey[i]] = [
        this.membersKey[i],
        this.membersKey[0]
      ];
      this.handlers.trackHandler.reloadTracks();
    }

    console.error(this.membersKey);
  };

  /**
   * switch device to send data
   */
  switchDevice = () => {
    this.handlers.trackHandler.switchVideo();
  };
}

window.Conference = Conference;
