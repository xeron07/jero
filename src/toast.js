import * as toastr from "toastr";

export default class Toast {
  constructor() {
    this.toast = toastr;
    this.iconLink = {
      warning:
        "https://res.cloudinary.com/emerging-it/image/upload/v1578384490/toast-icons/warning_eywapp.png",
      error:
        "https://res.cloudinary.com/emerging-it/image/upload/v1578384651/toast-icons/error_hwgmfs.png",
      success:
        "https://res.cloudinary.com/emerging-it/image/upload/v1578378896/toast-icons/success_naq9jr.png",
      info:
        "https://res.cloudinary.com/emerging-it/image/upload/v1578378896/toast-icons/info_nrobck.png"
    };
    this.toast.options = this.toastrOption();
  }

  toastrOption = () => {
    let optionsData = {
      closeButton: false,
      debug: false,
      newestOnTop: true,
      progressBar: true,
      positionClass: "toast-bottom-left",
      preventDuplicates: false,
      onclick: null,
      showDuration: "300",
      hideDuration: "1000",
      timeOut: "3000",
      extendedTimeOut: "1000",
      showEasing: "linear",
      hideEasing: "swing",
      showMethod: "slideDown",
      hideMethod: "fadeOut"
    };

    return optionsData;
  };
  warning = m => {
    this.toast.warning("<i class='far fa-bell'></i>", m);
  };

  error = m => {
    this.toast.error("<i class='far fa-bell'></i>", m);
  };

  success = m => {
    this.toast.success("<i class='far fa-bell'></i>", m);
  };

  info = m => {
    console.warn("here");
    console.error(this.toast);
    this.toast.info("<i class='far fa-bell'></i>", m);
  };
}
