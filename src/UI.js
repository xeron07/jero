import * as Lib from "./lib";
export default class UI {
  constructor(l) {
    this.lib = l;
  }

  sUI = t => {
    let size = this.lib.getSize();
    console.error(size);
    return (
      ' <div class="container-fluid" style="height:' +
      size.height +
      ",width:" +
      size.width +
      '"><div id="mainBlock">' +
      ' <div id="moderator-video">' +
      '<video id="moderator" autoplay="1">' +
      ' <source src="youtube.mp4" type="video/mp4" /></video><audio id="mAudio"></audio></div>' +
      '<div id="local-video">' +
      '<video id="local">' +
      ' <source src="youtube.mp4" type="video/mp4" />' +
      "</video>" +
      "</div>" +
      ' <div id="navBlock"><div id="nav"><button class="btn btn-secondary">Connect</button>' +
      ' <button class="btn" onclick="t.trackPlayToggle(this)">' +
      '<i class="fas fa-play-circle" style="color: whitesmoke;font-size: 20px;"></i>' +
      "</button>" +
      '<button class="btn" onclick="t.muteToggle(this)">' +
      '<i class="fas fa-microphone-alt-slash" style="color: whitesmoke;font-size: 20px;"></i>' +
      `<button class="btn" onclick="jeroTeacher_kickUser(${id})" >` + // onclick="t.trackStop()"
      '<i class="fas fa-times-circle" style="color: red;font-size: 20px;"></i>' +
      '<button class="btn" >' + //onclick="toggleFullscreen()"
      '<i class="fas fa-arrows-alt" style="color: whitesmoke;font-size: 20px;"></i>' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
    );
  };

  sUIStyle = () => {
    return (
      ".container {" +
      " background-color: rgb(96, 96, 96);" +
      "}" +
      "#mainBlock {" +
      "background-color: transparent;" +
      "position: relative;" +
      "top: 0;" +
      "left: 0;" +
      "width: 80%;" +
      "padding: 0px;" +
      "}" +
      "#moderator-video {" +
      "position: relative;" +
      "line-height: 0;" +
      "top: 0%;" +
      "left: 0;" +
      "max-height:60%;" +
      "margin: auto;" +
      "}" +
      "#local-video {" +
      "position: absolute;" +
      "width: 15%;" +
      "right: 15;" +
      "top: 15;" +
      "}" +
      "#actionButtons {" +
      "display: inline;" +
      "margin-left: 30%;" +
      "}" +
      "video {" +
      "width: 100% !important;" +
      "height: auto !important;" +
      "border: 2px solid transparent;" +
      "border-radius: 5px;" +
      "border-color: hotpink;" +
      "left: 0;" +
      "top: 0;" +
      "}" +
      "#navBlock {" +
      "background-color: transparent;" +
      "position: absolute;" +
      "width: 90%;" +
      "height: auto;" +
      "left: 5%;" +
      "right: 5%;" +
      "bottom: 5%;" +
      "}" +
      "#nav-div {" +
      "position: relative;" +
      "}"
    );
  };

  teStyle = () => {
    return (
      " #mainBlock {" +
      "background-color: aqua;" +
      "position: absolute;" +
      " top: 0;" +
      "left: 0;" +
      "width: 100%;" +
      "padding: 0px;" +
      "padding: 0px;" +
      "}" +
      "#remoteVideos {" +
      " line-height: 0;" +
      " top: 0%;" +
      "left: 0;" +
      " margin: auto;" +
      "background-color: brown;" +
      "}" +
      " .video-box {" +
      " position: absolute;" +
      "  width: 49.5%;" +
      "height: 100%;" +
      "display: flex;" +
      "justify-content: center;" +
      "      align-items: center;" +
      "    }" +
      "  #local-video {" +
      "position: absolute;" +
      "  width: 15%;" +
      "right: 15;" +
      "top: 15;" +
      " }" +
      "#actionButtons {" +
      "display: inline;" +
      " margin-left: 30%;" +
      "}" +
      " #local {" +
      " width: 100% !important;" +
      "height: auto !important;" +
      " border: 2px solid transparent;" +
      " border-radius: 5px;" +
      "border-color: hotpink;" +
      " left: 0;" +
      "top: 0;" +
      " }" +
      "#navBlock {" +
      "background-color: transparent;" +
      "height: auto;" +
      " left: 5%;" +
      " right: 5%;" +
      " bottom: 5%;" +
      "}" +
      " #nav {" +
      " position: relative;" +
      " /* display: inline-block; */" +
      "/* align-items: center; */" +
      "   }" +
      "video {" +
      "width: 100% !important;" +
      "  height: auto !important;" +
      " border: 2px solid transparent;" +
      " border-radius: 5px;" +
      "left: 0;" +
      "top: 0;" +
      "   }" +
      " #buttons {" +
      " text-align: center;" +
      "position: relative;" +
      "left: 0;" +
      " bottom: 40;" +
      " }" +
      "  .video-box-big {" +
      " position: absolute;" +
      "width: 50%;" +
      "height: 100%;" +
      "display: flex;" +
      "right: 0;" +
      "justify-content: center;" +
      " align-items: center;" +
      "}"
    );
  };

  tUI = () => {
    let html = "";
    for (let j = 1; j < this.lib.members.length; j++) {
      if (this.lib.members[j] !== this.lib.userId) {
        html += this.addStudentsVideo(this.lib.members[j]);
      }
    }

    return html;
  };

  addStudentsVideo = id => {
    return (
      '<div class="col-sm-4 jeroTeacher-videoBox" style=" background-color: rgb(96,96,96);padding-left: 0px;padding-right: 0px;border: 2px solid red;position: relative;display: flex;justify-content: center;">' +
      '<video id="jeroS-' +
      id +
      '" autoplay style="background-color: black;width: 100% !important;height: auto !important;border: 2px solid transparent;border-radius: 5px;left: 0;top: 0;">' +
      '<source src="https://res.cloudinary.com/emerging-it/video/upload/v1578463007/loading-video/60FPS_Fast_Spinning_Circles_Element_Loading_Gadget_Animation_bny7vb.mp4" type="video/mp4">' +
      "</video>" +
      '<div id=jeroTeacher-buttons style="text-align: center;position: absolute;bottom: 10px;max-width: 100%;max-height: 30%;" >' +
      '<button class="btn responsive" ng-click="jeroTeacher_playPause()" style="max-width: 25%;max-height: 100%;">' +
      ' <i class="fas fa-play-circle" style="color: whitesmoke;font-size: 7px;"></i>' +
      "</button>" +
      `<button  class="btn responsive" ng-click="jeroTeacher_muteUnmute('${id}')" style="max-width: 25%;max-height: 100%;">` +
      '<i class="fas fa-microphone-alt-slash" style="font-size: 7px;color: whitesmoke;"></i>' +
      "</button>" +
      `<button class="btn responsive" (click)="jeroTeacher_kickUser(${id})" style="max-width: 25%;max-height: 100%;" >` +
      '<i class="fas fa-times-circle" style="color: whitesmoke;font-size: 7px;"></i>' +
      " </button>" +
      '<button class="btn responsive" onclick="toggleFullscreen()" style="max-width: 25%;max-height: 100%;" >' +
      ' <i class="fas fa-arrows-alt" style="font-size: 7px;color: whitesmoke;"></i>' +
      "  </button>" +
      "</div>" +
      "</div>"
    );
  };
}
