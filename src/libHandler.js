/**
 * importing dependency (toaster class , sweetalert)
 * only notification dependency added
 */

import Toast from "./toast";
import * as Swal from "sweetalert2";

/**
 * purpose of this class is to handle the conference and connection event
 */
export default class LibHandler {
  /**
   * @constructor
   * @param {*} obj is lib class object
   */

  constructor(obj) {
    //lib class object
    this.libObj = obj;

    //toast class object
    this.toast = new Toast();
  }

  /*
   * connection handler
   * @params this function doesn't need a parameter
   * this function only trigger when
   * failed to connect with server
   */
  connFailed = () => {
    this.toast.error("problem in connection\n Check your internet connection ");
  };

  /*
   * connection handler
   * @params no parameter
   * Only trigger when successfully connected to server successfully
   */
  connSuccess = () => {
    this.toast.success("Connected to server");
    this.libObj.roomCreate();
  };

  /*
   *  after connection handler
   *  @params no parameter
   *  Trigger when disconnect with server
   */
  connLost = () => {
    this.toast.error("connection lost");
  };

  /*
   * Conference handler
   * @params no parameter
   * When user succesfully join the conference
   */
  confJoined = () => {
    console.error("Conference Joined");
    this.toast.success("You joined the conference");
  };

  /*
   * Conferecne error handler
   * @params no parameter
   * Trigger when user failed to join conferecne
   */
  confFailed = () => {
    this.toast.error("You didn't joined the conference\n Try again.");
  };

  /*
   * After conferecne handler
   * @params no parameter
   * Trigger when user left the confernce
   */
  confLeft = () => {
    Swal.fire("Yay", "You left the conference.", "warning");
  };

  /*
   * Conference error handler
   * @params error information
   * Trigger when connected to server but problem in
   * joining the conference
   */
  confError = err => {
    Swal.fire("Oops!", "You are unable to join the conference" + err, "error");
  };
}
