/**
 * toaster class and  sweetalert added as dependency for notification purpose
 */

import * as Swal from "sweetalert2";
import Toast from "./toast";

/**
 * confercne participant event will be handle by this class
 * after successfully joinning the conference this event handler class will handle
 * different events which where @emits by the paritipants of the conference
 */
export default class UserHandler {
  /**
   *
   * @param {*} u is a lib class object
   */
  constructor(u) {
    this.raiseHand = null;
    this.tostr = new Toast();
    this.lib = u;
  }

  /**
   * this event will trigger when a new user in room
   * @params *id(user id) *u(user information object)
   */

  userAdded = (id, u) => {
    if (!this.lib.membersMap[id]) {
      this.lib.membersMap[id] = [];
    }

    if (this.lib.getRole() === "teacher" && !this.lib.membersKey[id]) {
      // this.lib.members = [];
      // let participantsData = this.lib.room.getParticipants();
      // participantsData.forEach(data => {
      //   if (data._connectionStatus === "active") {
      //     this.lib.members[data._id] = data;
      //   }
      // });

      //

      this.lib.members[id] = u;
      //updating memberskey(user id) array from members array of object
      this.lib.membersKey = Object.keys(this.lib.members);
    } else {
      if (u.isModerator() && this.lib.getModerator() === null) {
        console.error("student view id: " + id);
        this.lib.setModerator(id);

        this.lib.members[id] = u;
        console.warn(this.lib.members);
        //updating memberskey(user id) array from members array of object
        this.lib.membersKey = Object.keys(this.lib.members);
      }
    }

    console.warn(u);
    console.warn("finally");
    console.error(this.lib.membersKey);
    this.tostr.info("Someone Just Joined the conference");
    // setTimeout(() => {
    //   this.lib.handlers.trackHandler.reloadTracks();
    // }, 3000);
  };

  addParticipant = () => {};

  /**
   * @emits when any participant left or kicked from the conference
   * @param id(jitsi id ) & u(paricipant object)
   */
  userLeave = (id, u) => {
    // if (this.lib.membersMap[id]) {
    //  // this.waitForUser(id);
    // }

    console.error("someone left: " + id);
    // this.lib.reloadTracks();
    if (this.lib.getRole() === "teacher") {
      //filtering membersKey to erase the participant
      this.lib.membersKey = this.lib.membersKey.filter(data => {
        if (data != id) {
          return data;
        }
      });
      this.lib.members = [];

      //updating the member array
      for (let i = 0; i < this.lib.membersKey.length; i++) {
        this.lib.members[this.lib.membersKey[i]] = this.lib.members[
          this.lib.membersKey[i]
        ];
      }
    } else {
      if (this.lib.getModerator() === id) {
        this.lib.setModerator(null);
      }
    }

    // this.lib.members.delete(id);
    console.warn(u);
    this.lib.handlers.trackHandler.reloadTracks();
    this.tostr.info("someone left the conference");
  };

  /**
   * notify if any user kicked
   * @param user id or jid(jitsi id )
   * @type string
   */
  userKicked = id => {
    console.warn(
      "my user id: " + this.lib.room.myUserId() + " kicked user id: " + id
    );
    this.tostr.error("You have been kicked by teacher");
  };

  userMute = id => {
    this.tostr.info("someone has been muted");
  };

  raiseHand = id => {};

  shareScreen = () => {};
}
