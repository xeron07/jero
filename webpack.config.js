const path = require("path");
module.exports = {
  mode: "development",
  entry: {
    "app.js": ["./src/conference.js"]
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-env"],
          plugins: [
            ["@babel/plugin-proposal-class-properties", { loose: true }]
          ]
        }
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js"]
  },
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
    filename: "[name]"
  }
};
